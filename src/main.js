import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import './assets/main.scss'

Vue.config.productionTip = false
require('./assets/main.scss')

new Vue({
  router,
  axios,
  store,
  render: h => h(App)
}).$mount('#app')

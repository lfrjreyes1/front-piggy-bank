PIGGY BANK
Piggy Bank es un proyecto del funcionamiento de una alcancia
para una prueba tecnica para una empresa X.

Comenzando 🚀
repositorio GIT
git clone https://bitbucket.org/lfrjreyes1/front-piggy-bank/src/master/

Pre-requisitos 📋
Node
Npm


Despliegue 📦
se debe descargar el repositorio y luego subir
el servicio con el ide de su preferencia.
La aplicacion corre sobre el puerto 8080

Comandos
npm install
npm run serve

puede probar que la aplicacion esta arriba entrando al siguiente link
https://bitbucket.org/lfrjreyes1/front-piggy-bank/src/master/


Construido con 🛠️

Vue JS - El framework web usado
Node JS- Manejador de dependencias

Autor ✒️

Luis Fernando Reyes Jaraba
